﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Gronsfeld_Cipher
{
    public partial class MainForm : Form
    {
        GronsfeldCipher gronsfeld = new GronsfeldCipher();

        private bool keyEntered = false;
        private bool textEntered = false;

        public MainForm()
        {
            InitializeComponent();
            this.Select();
        }

        private void keyBox_Enter(object sender, EventArgs e)
        {
            if (!keyEntered)
            {
                keyBox.Text = "";
                keyEntered = true;
                keyBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void keyBox_Leave(object sender, EventArgs e)
        {
            if (keyBox.Text == "")
            {
                keyBox.Text = "Ключ...";
                keyBox.ForeColor = SystemColors.InactiveCaption;
                keyEntered = false;
            }
        }

        private void inTextBox_Enter(object sender, EventArgs e)
        {
            if (!textEntered)
            {
                inTextBox.Text = "";
                textEntered = true;
                inTextBox.ForeColor = SystemColors.WindowText;
            }
        }

        private void inTextBox_Leave(object sender, EventArgs e)
        {
            if (inTextBox.Text == "")
            {
                inTextBox.Text = "Исходный текст...";
                textEntered = false;
                inTextBox.ForeColor = SystemColors.InactiveCaption;
            }
        }

        private void encryptButton_Click(object sender, EventArgs e)
        {
            if (CheckRequired())
            {
                outTextBox.Text = gronsfeld.Encrypt(inTextBox.Text.ToLower(), keyBox.Text.ToLower());
            }
        }

        private void decryptButton_Click(object sender, EventArgs e)
        {
            if (CheckRequired())
            {
                outTextBox.Text = gronsfeld.Decrypt(inTextBox.Text.ToLower(), keyBox.Text.ToLower());
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            inTextBox.Clear();
            outTextBox.Clear();
            keyBox.Clear();
        }

        private bool CheckRequired()
        {
            if (!keyEntered && !textEntered)
            {
                MessageBox.Show("Введите ключ и исходный текст.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (!keyEntered || string.IsNullOrWhiteSpace(keyBox.Text))
            {
                MessageBox.Show("Введите ключ.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (!textEntered || string.IsNullOrWhiteSpace(inTextBox.Text))
            {
                MessageBox.Show("Введите исходный текст.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void keyBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsControl(e.KeyChar))
            {
                return;
            }
            if (!char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
                return;
            }
        }
    }
}
