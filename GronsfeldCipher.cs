﻿using System.Text.RegularExpressions;

namespace Gronsfeld_Cipher
{
    class GronsfeldCipher
    {
        private readonly string rusAlphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

        string text;

        public string Encrypt(string input, string key)
        {
            string encryptedText = "";
            text = input;
            string mKey = ModifyKey(key);

            int fault = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (!Regex.IsMatch(text[i].ToString(), @"\P{IsCyrillic}"))
                {
                    encryptedText += GetCharFromAlphabet((GetIndexInAlphabet(text[i]) + int.Parse(mKey[i - fault].ToString())) % rusAlphabet.Length);
                }
                else
                {
                    encryptedText += text[i];
                    fault++;
                }
            }

            return encryptedText;
        }

        public string Decrypt(string input, string key)
        {
            string decryptedText = "";
            text = input;
            string mKey = ModifyKey(key);

            int fault = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (!Regex.IsMatch(text[i].ToString(), @"\P{IsCyrillic}"))
                {
                    decryptedText += GetCharFromAlphabet((GetIndexInAlphabet(text[i]) + rusAlphabet.Length - int.Parse(mKey[i - fault].ToString())) % rusAlphabet.Length);
                }
                else
                {
                    decryptedText += text[i];
                    fault++;
                }
            }

            return decryptedText;
        }

        private string ModifyKey(string key)
        {
            string newkey = key;

            if (text.Length > key.Length)
            {
                int i = 0;
                while (text.Length != newkey.Length)
                {
                    newkey += key[i];
                    i++;
                    if (i == key.Length)
                        i = 0;
                }
            }

            else if (text.Length < key.Length)
                newkey = key.Substring(0, text.Length);

            return newkey;
        }

        private int GetIndexInAlphabet(char chr)
        {
            return rusAlphabet.IndexOf(chr);
        }

        private char GetCharFromAlphabet(int index)
        {
            return rusAlphabet[index];
        }
    }
}
