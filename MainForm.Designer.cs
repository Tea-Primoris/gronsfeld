﻿namespace Gronsfeld_Cipher
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inDataGroup = new System.Windows.Forms.GroupBox();
            this.ClearButton = new System.Windows.Forms.Button();
            this.decryptButton = new System.Windows.Forms.Button();
            this.encryptButton = new System.Windows.Forms.Button();
            this.keyBox = new System.Windows.Forms.TextBox();
            this.inTextBox = new System.Windows.Forms.TextBox();
            this.outDataGroup = new System.Windows.Forms.GroupBox();
            this.outTextBox = new System.Windows.Forms.TextBox();
            this.inDataGroup.SuspendLayout();
            this.outDataGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // inDataGroup
            // 
            this.inDataGroup.AutoSize = true;
            this.inDataGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.inDataGroup.Controls.Add(this.ClearButton);
            this.inDataGroup.Controls.Add(this.decryptButton);
            this.inDataGroup.Controls.Add(this.encryptButton);
            this.inDataGroup.Controls.Add(this.keyBox);
            this.inDataGroup.Controls.Add(this.inTextBox);
            this.inDataGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.inDataGroup.Location = new System.Drawing.Point(12, 12);
            this.inDataGroup.Name = "inDataGroup";
            this.inDataGroup.Padding = new System.Windows.Forms.Padding(0);
            this.inDataGroup.Size = new System.Drawing.Size(409, 213);
            this.inDataGroup.TabIndex = 0;
            this.inDataGroup.TabStop = false;
            this.inDataGroup.Text = "Входные данные";
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(298, 175);
            this.ClearButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(108, 23);
            this.ClearButton.TabIndex = 5;
            this.ClearButton.Text = "Очистить";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // decryptButton
            // 
            this.decryptButton.Location = new System.Drawing.Point(152, 175);
            this.decryptButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.decryptButton.Name = "decryptButton";
            this.decryptButton.Size = new System.Drawing.Size(140, 23);
            this.decryptButton.TabIndex = 4;
            this.decryptButton.Text = "Расшифровать";
            this.decryptButton.UseVisualStyleBackColor = true;
            this.decryptButton.Click += new System.EventHandler(this.decryptButton_Click);
            // 
            // encryptButton
            // 
            this.encryptButton.Location = new System.Drawing.Point(6, 175);
            this.encryptButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.encryptButton.Name = "encryptButton";
            this.encryptButton.Size = new System.Drawing.Size(140, 23);
            this.encryptButton.TabIndex = 3;
            this.encryptButton.Text = "Зашифровать";
            this.encryptButton.UseVisualStyleBackColor = true;
            this.encryptButton.Click += new System.EventHandler(this.encryptButton_Click);
            // 
            // keyBox
            // 
            this.keyBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.keyBox.Location = new System.Drawing.Point(6, 21);
            this.keyBox.Name = "keyBox";
            this.keyBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.keyBox.Size = new System.Drawing.Size(400, 22);
            this.keyBox.TabIndex = 1;
            this.keyBox.Text = "Ключ...";
            this.keyBox.Enter += new System.EventHandler(this.keyBox_Enter);
            this.keyBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keyBox_KeyPress);
            this.keyBox.Leave += new System.EventHandler(this.keyBox_Leave);
            // 
            // inTextBox
            // 
            this.inTextBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.inTextBox.Location = new System.Drawing.Point(6, 49);
            this.inTextBox.Multiline = true;
            this.inTextBox.Name = "inTextBox";
            this.inTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.inTextBox.Size = new System.Drawing.Size(400, 120);
            this.inTextBox.TabIndex = 2;
            this.inTextBox.Text = "Исходный текст...";
            this.inTextBox.Enter += new System.EventHandler(this.inTextBox_Enter);
            this.inTextBox.Leave += new System.EventHandler(this.inTextBox_Leave);
            // 
            // outDataGroup
            // 
            this.outDataGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.outDataGroup.Controls.Add(this.outTextBox);
            this.outDataGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.outDataGroup.Location = new System.Drawing.Point(12, 231);
            this.outDataGroup.Name = "outDataGroup";
            this.outDataGroup.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.outDataGroup.Size = new System.Drawing.Size(409, 156);
            this.outDataGroup.TabIndex = 1;
            this.outDataGroup.TabStop = false;
            this.outDataGroup.Text = "Выходные данные";
            // 
            // outTextBox
            // 
            this.outTextBox.Location = new System.Drawing.Point(6, 21);
            this.outTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.outTextBox.Multiline = true;
            this.outTextBox.Name = "outTextBox";
            this.outTextBox.ReadOnly = true;
            this.outTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outTextBox.Size = new System.Drawing.Size(400, 120);
            this.outTextBox.TabIndex = 6;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(456, 414);
            this.Controls.Add(this.outDataGroup);
            this.Controls.Add(this.inDataGroup);
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Шифр Гронсфельда";
            this.inDataGroup.ResumeLayout(false);
            this.inDataGroup.PerformLayout();
            this.outDataGroup.ResumeLayout(false);
            this.outDataGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox inDataGroup;
        private System.Windows.Forms.GroupBox outDataGroup;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button decryptButton;
        private System.Windows.Forms.Button encryptButton;
        private System.Windows.Forms.TextBox keyBox;
        private System.Windows.Forms.TextBox inTextBox;
        private System.Windows.Forms.TextBox outTextBox;
    }
}

